import { Row, Column } from '../components/UI/Container'
import { StyledCheckbox } from '../components/UI/StyledInput'
import { NormalText } from '../components/UI/StyledText'
import { CoolButton, ButtonIcon } from '../components/UI/CoolButton'

import { FaTrashAlt } from 'react-icons/fa'

const TodoList = ({ items, handleCheck, handleDelete }) => {
  return (
    <>
      {
        items.length === 0
        ? (
          <Row>
            <Column>
              <NormalText color='#398C09'>Nothing to do today!</NormalText>
            </Column>
          </Row>
        )
        : items.map(item => (
          <Row key={item.id}>
            <Column cols={1}/>
            <Column cols={5}>
              <StyledCheckbox
                checkId={'check' + item.id}
                color='#5C098C' text={item.text}
                checked={item.completed}
                onChange={(e) => handleCheck(e, item.id)}
              />
            </Column>
            <Column cols={1}>
              <CoolButton
                color='#8C1A09'
                onClick={() => handleDelete(item.id)}
              >
                <ButtonIcon>
                  <FaTrashAlt />
                </ButtonIcon>
              </CoolButton>
            </Column>
            <Column cols={5}/>
          </Row>
        ))
      }
    </>
  )
}

export default TodoList