import styled from 'styled-components'

export const HeaderText = styled.h1`
  display: block;
  font-weight: bold;
  font-size: 2rem;
  color: ${props => props.color || 'black'};
  margin: 0;
`

export const NormalText = styled.span`
  display: inline-block;
  font-size: 1rem;
  color: ${props => props.color || 'black'};
  padding: 0.45rem 0.35rem;
  letter-spacing: -0.05em;
  word-spacing: -0.05em;
`