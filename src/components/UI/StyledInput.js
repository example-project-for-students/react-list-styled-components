import { useRef, useEffect } from 'react'

import styled from 'styled-components'
import { NormalText } from './StyledText'

const TextInput = styled.input.attrs({
  type: "text"
})`
  font-size: 1rem;
  font-family: inherit;
  padding: 0.25em 0.5em;
  background-color: #fff;
  border: 1px solid ${props => props.borderColor || 'black'};
  border-radius: 4px;

  &:focus {
    border: 2px solid ${props => props.borderColor || 'black'};
    outline: none;
  }
`

const CheckboxLabel = styled.label.attrs({
  className: 'checkbox-label'
})`
  display: inline-block;
  cursor: pointer;
  position: relative;
  padding-left: 25px;
  margin: 10px 0;
  color: ${props => props.color || 'black'};

  &::before, &::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
  }

  &::before {
    display: block;
    width: 20px;
    height: 20px;
    border: 2px solid ${props => props.color || 'black'};
  }

  &::after {
    display: none;
    width: 12px;
    height: 12px;
    margin: 4px;
    background-color: ${props => props.color || 'black'};
  }
`

const Checkbox = styled.input.attrs({
  type: "checkbox"
})`
  position: absolute;
  left: -9999px;

  &:focus {
    & + .checkbox-label::before {
      border-color: ${props => props.color || 'black'};
    }
  }  

  &:checked {
    & + .checkbox-label::after {
      display: block;
    }
  }
`

export const StyledTextInput = props => {
  const inputEl = useRef(null)

  useEffect(() => {
    inputEl.current.focus()
  }, [props.isFocusChanged])

  return (
    <label>
      <NormalText color={props.color}>{props.text}</NormalText>
      <TextInput {...props} ref={inputEl} />
    </label>
  )
}

export const StyledCheckbox = props => {
  return (
    <>
      <Checkbox id={props.checkId} {...props}/>
      <CheckboxLabel htmlFor={props.checkId} color={props.color}>{props.text}</CheckboxLabel>
    </>
  )
}