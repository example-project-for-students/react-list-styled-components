import styled from 'styled-components'

const columnsNumber = 12

export const Container = styled.div`
  max-width: 800px;
  margin: 20px auto;
  ${props => props.bordered && 
    `border: 2px solid black;
     border-radius: 10px;`}
  ${props => props.raised && 'box-shadow: 10px 5px 5px black;'}
`

export const Row = styled.div`
  width: 100%;
  display: flex;
  flex-flow: row no-wrap;
  align-items: center;
  margin: 0.5rem 0;
`

export const Column = styled.div`
  width: ${props => (props.cols / columnsNumber * 100) || 100}%;
  padding: 0 0.5rem;
`