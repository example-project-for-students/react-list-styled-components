import styled from 'styled-components'

export const Chip = styled.span`
  display: inline-block;
  font: inherit;
  border: none;
  border-radius: 15px;
  background-color: ${props => props.color};
  color: ${props => props.lightColor ? 'black' : 'white'};
  padding: 0.1rem 0.5rem;
  margin: 4px;
`