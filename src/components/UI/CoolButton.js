import styled from 'styled-components'

const ButtonWrapper = styled.div`
  display: inline-block;
  margin: 4px 10px;
`

const BaseButton = styled.button`
  font: inherit;
  border: ${props => props.lightColor ? '1px solid black' : 'none'};
  border-radius: 6px;
  background-color: ${props => props.color};
  color: ${props => props.lightColor ? 'black' : 'white'};
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  padding: 0.5rem 0.75rem;
  cursor: pointer;
  transition: all 0.2s;
  outline-offset: 2px;
  outline: ${props => props.active ? '2px solid ' + props.color : 'none'};

  ${props => props.text && `
    .btn-icon {
      margin-left: 5px;
    }
  `}

  ${props => props.small && `
    font-size: 0.8rem;
    .btn-icon {
      width: 16px;
      height: 16px;
    }
  `}
  ${props => props.large && `
    font-size: 1.5rem;
    .btn-icon {
      width: 30px;
      height: 30px;
    }
  `}
  &:hover {
    opacity: 0.6;
  }
`

const BaseOutlineButton = styled(BaseButton)`
  background-color: white;
  border: 1px solid ${props => props.color};
  color: ${props => props.color};

  &:hover {
    background-color: ${props => props.color};
    color: white;
    opacity: 1;
  }
`

export const ButtonIcon = styled.div.attrs({
  className: 'btn-icon'
})`
  width: 20px;
  height: 20px;

  img {
    width: 100%;
    height: 100%;
  }
`

export const CoolButton = props => {
  return (
    <ButtonWrapper>
      {
        props.outline
        ? <BaseOutlineButton {...props}>
            {props.text && <span>{props.text}</span>}
            {props.children}
          </BaseOutlineButton>
        : <BaseButton {...props}>
            {props.text && <span>{props.text}</span>}
            {props.children}
          </BaseButton>
      }
    </ButtonWrapper>
  )
}