import { Row, Column } from '../components/UI/Container'
import { HeaderText } from '../components/UI/StyledText'

const TodoListHeader = () => {
  return (
    <Row>
      <Column>
        <HeaderText color='#398C09'>Todo List - Styled</HeaderText>
      </Column>
    </Row>
  )
}

export default TodoListHeader