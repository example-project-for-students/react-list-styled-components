import { useState } from 'react'

import { Row, Column } from '../components/UI/Container'
import { Chip } from '../components/UI/Chip'
import { CoolButton, ButtonIcon } from '../components/UI/CoolButton'

import { FaFilter } from 'react-icons/fa'

const TodoListFooter = ( { nrItemsLeft, filterItems }) => {
  const [activeButtons, setActiveButtons] = useState([true, false])

  const handleFilter = filter => {
    if (filter) {
      setActiveButtons([false, true])
    } else {
      setActiveButtons([true, false])
    }
    filterItems(filter)
  }

  return (
    <Row>
      <Column cols={1} />
      <Column cols={3}>
        <Chip color='#097B8C'>{`${nrItemsLeft} items left`}</Chip>
      </Column>
      <Column cols={9}>
        <CoolButton
          color='#097B8C' text='Show all'
          active={activeButtons[0]}
          onClick={() => handleFilter(false)}
        />
        <CoolButton 
          color='#097B8C' text='Show active'
          active={activeButtons[1]}
          onClick={() => handleFilter(true)}
        >
          <ButtonIcon>
            <FaFilter />
          </ButtonIcon>
        </CoolButton>
      </Column>
    </Row>
  )
}

export default TodoListFooter