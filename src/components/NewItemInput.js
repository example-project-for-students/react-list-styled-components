import { useState } from 'react'

import { Row, Column } from '../components/UI/Container'
import { StyledTextInput } from '../components/UI/StyledInput'
import { CoolButton, ButtonIcon } from '../components/UI/CoolButton'

import { FaPlus } from 'react-icons/fa'

const NewItemInput = ( { addTodo }) => {
  const [newTodoText, setNewTodoText] = useState('')
  const [isInputFocusChanged, setIsInputFocusChanged] = useState(false)

  const handleChange = e => setNewTodoText(e.target.value)

  const handleSubmit = (e, text) => {
    e.preventDefault()
    setNewTodoText('')
    setIsInputFocusChanged(!isInputFocusChanged)
    addTodo(text)
  }

  return (
    <Row>
      <Column cols={1}/>
      <Column cols={11}>
        <form onSubmit={(e) => handleSubmit(e, newTodoText)}>
          <StyledTextInput
            text='Add new task:' color='#398C09' borderColor='#398C09'
            isFocusChanged={isInputFocusChanged}
            value={newTodoText}
            onChange={handleChange}
          />
          <CoolButton type="submit" color='#398C09' text='Add' outline>
            <ButtonIcon>
              <FaPlus />
            </ButtonIcon>
          </CoolButton>
        </form>
      </Column>
    </Row>
  )
}

export default NewItemInput